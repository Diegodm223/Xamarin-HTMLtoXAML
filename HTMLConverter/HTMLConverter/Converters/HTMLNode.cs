﻿using System;
using System.Collections.Generic;
using System.Text;
using HtmlAgilityPack;
using Xamarin.Forms;

namespace HTMLConverter.Converters
{
    class HTMLNode
    {
        private HtmlNode node;
        private HTMLStyle style;
        private StackLayout stack;

        internal HTMLNode(HtmlNode node)
        {
            this.node = node;
            this.style = null;
           // stack.Children.Add()
        }

        internal HTMLNode(HtmlNode node, HTMLStyle style) {
            this.node = node;
            this.style = style;
        }
    }
}
