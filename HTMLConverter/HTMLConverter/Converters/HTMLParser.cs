﻿using System;
using System.Collections.Generic;
using System.Text;
using HtmlAgilityPack;
using Xamarin.Forms;
using HTMLConverter.Converters.Styles;

namespace HTMLConverter.Converters
{
    class HTMLParser
    {       
        private Dictionary<string, GestureRecognizer> elementGestures = new Dictionary<string, GestureRecognizer>();
        private View Body;


        internal View Parse(string htmlString) {
            Body = new StackLayout();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlString);
            try
            {
                return ParseElement(doc.DocumentNode);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("ERROR. Error parsing HTML document");
            }
            return null;
        }

        internal View Parse(string htmlString, Dictionary<string, GestureRecognizer> gesturesDictionary)
        {
            elementGestures = gesturesDictionary;
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlString);
            try {              
                return ParseElement(doc.DocumentNode);
            } catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine("ERROR. Error parsing HTML document");
            }
            return null;
        }


        private View ParseElement(HtmlNode node) {
            switch (node.Name) {
                // Blocks and sections:
                case "#document":
                    return ParseElement(node.FirstChild);
                case "html":
                case "body":
                case "blockquote":
                case "caption":
                case "div":
                case "form":
                    return ParseNodeSection(node);

                // Text:
                case "p":
                case "h1":
                case "h2":
                case "h3":
                case "h4":
                case "h5":
                case "h6":
                case "span":
                    return ParseNodeText(node);

                case "ol":
                    return ParseOrderedList(node);
                case "ul":
                    return ParseUnorderedList(node);
                case "dir": //  treat as UL element
                case "menu": //  treat as UL element
                             // List element conversion
                    ///AddList(xamlParentElement, htmlElement, inheritedProperties, stylesheet, sourceContext);
                    break;
                case "li":
                    // LI outside of OL/UL
                    // Collect all sibling LIs, wrap them into a List and then proceed with the element following the last of LIs
                    ///htmlNode = AddOrphanListItems(xamlParentElement, htmlElement, inheritedProperties, stylesheet, sourceContext);
                    return ParseListItem(node);

                case "img":
                   // AddImage(xamlParentElement, htmlElement, inheritedProperties, stylesheet, sourceContext);
                    break;

                case "table":
                    // hand off to table parsing function which will perform special table syntax checks
                    // DISABLE TABLES (it seems like they don't work most of the time)
                    // AddTable(xamlParentElement, htmlElement, inheritedProperties, stylesheet, sourceContext);
                    break;

                case "tbody":
                case "tfoot":
                case "thead":
                case "tr":
                case "td":
                case "th":
                    // Table stuff without table wrapper
                    // TODO: add special-case processing here for elements that should be within tables when the
                    // parent element is NOT a table. If the parent element is a table they can be processed normally.
                    // we need to compare against the parent element here, we can't just break on a switch
                    goto default; // Thus we will skip this element as unknown, but still recurse into it.

                case "style": // We already pre-processed all style elements. Ignore it now
                case "meta":
                case "head":
                case "title":
                case "script":
                    // Ignore these elements
                    return null;
               
                default:
                    // Wrap a sequence of inlines into an implicit paragraph
                    // htmlNode = AddImplicitParagraph(xamlParentElement, htmlElement, inheritedProperties, stylesheet, sourceContext);
                    return null;
            }
            return null;
        }

        private View ParseNodeSection(HtmlNode node) {
            StackLayout div = new StackLayout();
            foreach (var child in node.ChildNodes) {
                div.Children.Add(ParseElement(child));
            }
            return div;
        }

        private View ParseNodeText(HtmlNode node)
        {            
            Label textNode = new Label();
            textNode.Text = node.InnerText;
            switch (node.Name) {
                case "p":
                    XAMLStyle.ApplyStyle(textNode, HTMLConverter.Style.Html_p);
                    break;
                case "span":
                    XAMLStyle.ApplyStyle(textNode, HTMLConverter.Style.Html_span);
                    break;
                case "h1":
                    XAMLStyle.ApplyStyle(textNode, HTMLConverter.Style.Html_h1);
                    break;
                case "h2":
                    XAMLStyle.ApplyStyle(textNode, HTMLConverter.Style.Html_h2);
                    break;
                case "h3":
                    XAMLStyle.ApplyStyle(textNode, HTMLConverter.Style.Html_h3);
                    break;
                case "h4":
                    XAMLStyle.ApplyStyle(textNode, HTMLConverter.Style.Html_h4);
                    break;
                case "h5":
                    XAMLStyle.ApplyStyle(textNode, HTMLConverter.Style.Html_h5);
                    break;
                case "h6":
                    XAMLStyle.ApplyStyle(textNode, HTMLConverter.Style.Html_h6);
                    break;
                default:
                    XAMLStyle.ApplyStyle(textNode, HTMLConverter.Style.Html_p);
                    break;
            }

            return textNode;
        }

        private View ParseUnorderedList(HtmlNode node) {
            
            if (node.Name != "ul") return null;
            StackLayout st = new StackLayout {
                Margin = new Thickness(5,0,0,0)
            };

            foreach (var li in node.ChildNodes) {                
                st.Children.Add(ParseListItem(li));
            }

            return st;
        }

        private View ParseOrderedList(HtmlNode node)
        {

            if (node.Name != "ol") return null;
            StackLayout st = new StackLayout
            {
                Margin = new Thickness(5, 0, 0, 0)
            };
            int initialOrder = 1;
            foreach (var li in node.ChildNodes)
            {                
                st.Children.Add(ParseListItem(li, initialOrder));
                initialOrder++;
            }
           
            return st;
        }

        private View ParseListItem(HtmlNode node)
        {
            if (node.Name != "li") return null;
            Label li = new Label
            {
                Text = "•  " + node.InnerText
            };
            XAMLStyle.ApplyStyle(li, HTMLConverter.Style.Html_li);
            return li;
        }

        private View ParseListItem(HtmlNode node, int order)
        {
            if (node.Name != "li") return null;
            Label li = new Label
            {
                Text = order + "  " + node.InnerText
            };
            XAMLStyle.ApplyStyle(li, HTMLConverter.Style.Html_li);
            return li;
        }

        private View ParseListItem(HtmlNode node, string order)
        {
            if (node.Name != "li") return null;
            Label li = new Label
            {
                Text = order + "  " + node.InnerText
            };
            XAMLStyle.ApplyStyle(li, HTMLConverter.Style.Html_li);
            return li;
        }



        /*
         Styles
             */

       
    }
}
