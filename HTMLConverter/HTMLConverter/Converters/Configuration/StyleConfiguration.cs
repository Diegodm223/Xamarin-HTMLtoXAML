﻿using HTMLConverter.Converters.Styles;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HTMLConverter.Converters.Configuration
{
    public class StyleConfiguration
    {
        
        private XAMLStyle html_p = new XAMLStyle
        {
            FontAtributes = FontAttributes.None,
            FontSize = 12,
            Margin = new Thickness(0),
            TextColor = Color.Black
        };
        private XAMLStyle html_span = new XAMLStyle
        {
            FontAtributes = FontAttributes.None,
            FontSize = 12,
            Margin = new Thickness(0),
            TextColor = Color.Black
        };
        private XAMLStyle html_h6 = new XAMLStyle
        {
            FontAtributes = FontAttributes.Bold,
            FontSize = 14,
            Margin = new Thickness(0),
            TextColor = Color.Black
        };
        private XAMLStyle html_h5 = new XAMLStyle
        {
            FontAtributes = FontAttributes.Bold,
            FontSize = 16,
            Margin = new Thickness(0),
            TextColor = Color.Black
        };
        private XAMLStyle html_h4 = new XAMLStyle
        {
            FontAtributes = FontAttributes.Bold,
            FontSize = 18,
            Margin = new Thickness(0),
            TextColor = Color.Black
        };
        private XAMLStyle html_h3 = new XAMLStyle
        {
            FontAtributes = FontAttributes.Bold,
            FontSize = 20,
            Margin = new Thickness(0),
            TextColor = Color.Black
        };
        private XAMLStyle html_h2 = new XAMLStyle
        {
            FontAtributes = FontAttributes.Bold,
            FontSize = 22,
            Margin = new Thickness(0),
            TextColor = Color.Black
        };
        private XAMLStyle html_h1 = new XAMLStyle
        {
            FontAtributes = FontAttributes.Bold,
            FontSize = 24,
            Margin = new Thickness(0),
            TextColor = Color.Black
        };

        private XAMLStyle html_li = new XAMLStyle
        {
            FontAtributes = FontAttributes.None,
            FontSize = 12,
            Margin = new Thickness(5, 0, 0, 0),
            TextColor = Color.Black
        };


        
        public StyleConfiguration()
        {

        }

        /*
         PROPERTIES
         */
        public XAMLStyle Html_p { get => html_p; set => html_p = value; }
        public XAMLStyle Html_span { get => html_span; set => html_span = value; }
        public XAMLStyle Html_h6 { get => html_h6; set => html_h6 = value; }
        public XAMLStyle Html_h5 { get => html_h5; set => html_h5 = value; }
        public XAMLStyle Html_h4 { get => html_h4; set => html_h4 = value; }
        public XAMLStyle Html_h3 { get => html_h3; set => html_h3 = value; }
        public XAMLStyle Html_h2 { get => html_h2; set => html_h2 = value; }
        public XAMLStyle Html_h1 { get => html_h1; set => html_h1 = value; }
        public XAMLStyle Html_li { get => html_li; set => html_li = value; }
    }
}
