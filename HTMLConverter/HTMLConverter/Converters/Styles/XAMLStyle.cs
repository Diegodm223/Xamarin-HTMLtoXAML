﻿using System;
using Xamarin.Forms;

namespace HTMLConverter.Converters.Styles
{
    public class XAMLStyle
    {
        // Container
        private Thickness margin = new Thickness(0);
        private Thickness padding = new Thickness(0);
        private string width = "";
        private string height = "";
        private string cssFloat = "";                    //rigth or left
        private Color backgroundColor = Color.White;

        // Border
        private double BorderSize = 0;
        private Color BorderColor = Color.White;


        // Text
        private double fontSize;
        private FontAttributes fontAtributes = FontAttributes.None;
        private string fontFamily = "";        
        private Color textColor = Color.Black;
        private TextAlignment textAlign = TextAlignment.Start;

        // Images
        
       
        public XAMLStyle()
        {

        }

        public Thickness Margin { get => margin; set => margin = value; }
        public double MarginLeft { get => margin.Left; set => margin.Left = value; }
        public double MarginTop { get => margin.Top; set => margin.Top = value; }
        public double MarginRight { get => margin.Right; set => margin.Right = value; }
        public double MarginBottom { get => margin.Bottom; set => margin.Bottom = value; }
        public Thickness Padding { get => padding; set => padding = value; }
        public double PaddingLeft { get => padding.Left; set => padding.Left = value; }
        public double PaddingTop { get => padding.Top; set => padding.Top = value; }
        public double PaddingRigth { get => padding.Right; set => padding.Right = value; }
        public double PaddingBottom { get => padding.Bottom; set => padding.Bottom = value; }
        public string Width { get => width; set => width = value; }
        public string Height { get => height; set => height = value; }
        public string CssFloat { get => cssFloat; set => cssFloat = value; }
        public Color BackgroundColor { get => backgroundColor; set => backgroundColor = value; }
        public double BorderSize1 { get => BorderSize; set => BorderSize = value; }
        public Color BorderColor1 { get => BorderColor; set => BorderColor = value; }
        public double FontSize { get => fontSize; set => fontSize = value; }
        public FontAttributes FontAtributes { get => fontAtributes; set => fontAtributes = value; }
        public string FontFamily { get => fontFamily; set => fontFamily = value; }
        public Color TextColor { get => textColor; set => textColor = value; }
        public TextAlignment TextAlign { get => textAlign; set => textAlign = value; }


        /// <summary>
        /// Apply the style to a Label element
        /// </summary>
        /// <param name="style"></param>
        /// <param name="label"></param>
        internal static void ApplyStyle(Label label, XAMLStyle style)
        {
            label.FontAttributes = style.FontAtributes;
            if (style.FontFamily != "")
                label.FontFamily = style.FontFamily;
            label.FontSize = style.FontSize;
            label.Margin = style.Margin;
            label.TextColor = style.TextColor;
            label.BackgroundColor = style.BackgroundColor;
        }

        /// <summary>
        /// Apply the style to a Stack element
        /// </summary>
        /// <param name="label"></param>
        /// <param name="style"></param>
        internal static void ApplyStyle(StackLayout stack, XAMLStyle style)
        {
            stack.Margin = style.Margin;
            stack.Padding = style.Padding;

            if (style.width != "") {
                style.width.Replace("px","");
                try {
                    double w = double.Parse(style.width);
                    stack.WidthRequest = w;
                } catch (Exception) { }
            }

            if (style.height != "")
            {
                style.height.Replace("px", "");
                try
                {
                    double h = double.Parse(style.height);
                    stack.HeightRequest = h;
                }
                catch (Exception) { }
            }

            stack.BackgroundColor = style.BackgroundColor;
            
        }
    }
}
