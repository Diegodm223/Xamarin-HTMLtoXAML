﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTMLConverter
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HTMLWebView : ContentPage
	{
		public HTMLWebView (string html)
		{
			InitializeComponent ();

            WebView wb = new WebView
            {
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.Fill,
                Source = new HtmlWebViewSource
                {
                    Html = html
                }
            };

            this.Content = wb;
		}
	}
}