﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HTMLConverter
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TestHTML : ContentPage
	{
        private static string HTML_TEST = "<html><body><h2>An Unordered HTML List</h2><ul><li>Coffee</li><li>Tea</li><li>Milk</li></ul><h2>An Ordered HTML List</h2><ol><li>Coffee</li><li>Tea</li><li>Milk</li></ol></body></html>";

		public TestHTML ()
		{
			InitializeComponent ();

            Button btnVerHTML = new Button {
                Text = " Ver HTML ",               
            };

            btnVerHTML.Clicked += (sender, args) => {
                Navigation.PushAsync(new HTMLWebView(HTML_TEST));
            };

            _stack.Children.Add(btnVerHTML);
            
            var parsedHTML = HTMLConverter.ParseHTML(HTML_TEST);

            _stack.Children.Add(parsedHTML);

        }
	}
}