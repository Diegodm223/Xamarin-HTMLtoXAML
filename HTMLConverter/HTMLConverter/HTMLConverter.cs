﻿using HTMLConverter.Converters.Configuration;
using HTMLConverter.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HTMLConverter
{
    public class HTMLConverter
    {
        private StyleConfiguration style = null;
        private HTMLParser parser = null;

        //Properties

        internal HTMLParser Parser { get => parser; set => parser = value; }

        // Singleton implementation
        private static HTMLConverter current = null;
        internal static HTMLConverter Current
        {
            get
            {
                if (current == null || current.style == null || current.parser == null)
                {
                    Init();
                }
                return current;
            }
            set { current = value; }
        }




        /* PUBLIC INTERFACE */
        public static void Init()
        {
            current = new HTMLConverter();
            current.style = new Converters.Configuration.StyleConfiguration();
            current.parser = new HTMLParser();
        }

        // NEXT FUNCTIONS MUST BE CALLED AFTER INIT

        public static View ParseHTML(string html)
        {
            return Current?.Parser?.Parse(html);
        }

        public static StyleConfiguration Style { get => Current.style; set => Current.style = value; }
    }
}
